This is a CPP Application that uses the following algorithms to generate the next move for a computer to play in the game of Othello.

* Min-Max algorithm
* Alpha-Beta Pruning