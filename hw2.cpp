#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <utility>
#include <queue>
#include <numeric>
#include <sstream>
using namespace std;

class compareIndex{
public:
	bool operator()(const pair<int,int> &n1, const pair<int,int> &n2) const{
		if(n1.first != n2.first){
			return n1.first > n2.first;
		} else {
			return n1.second > n2.second;
		}
	}
};

int minMove(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int minX,int minY,fstream &ofile);
int maxMove(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int maxX,int maxY,fstream &ofile);
int minMoveAlphaBeta(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int minX,int minY,int alpha,int beta,fstream &ofile);
int maxMoveAlphaBeta(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int maxX,int maxY,int alpha,int beta,fstream &ofile);

int max(int x,int y){
	return x>y?x:y;
}

int min(int x,int y){
	return x<y?x:y;
}

void printLog(string ycord,int xcord, int depth,int value,fstream &ofile){
	ostringstream s;
	string outputString;
	if(xcord == -1)
		s.str("");
	else
		s << fixed << xcord;
	string node="";
	node.append(ycord).append(s.str());
	s.str("");
	s<<fixed<<(depth);
	node.append(","+s.str());
	s.str("");
	outputString += node;
	if(value == (-2147483647))
		outputString+= ",-Infinity\n";
	else if(value == 2147483647)
		outputString+= ",Infinity\n";
	else{
		s<<fixed<<value;
		outputString+= ","+s.str() +"\n";
	}
	ofile<<outputString;
}

void printLogAlphaBeta(string ycord,int xcord, int depth,int value,int alpha,int beta,fstream &ofile){
	ostringstream s;
	string outputString;
	if(xcord == -1)
		s.str("");
	else
		s << fixed << xcord;
	string node="";
	node.append(ycord).append(s.str());
	s.str("");
	s<<fixed<<(depth);
	node.append(","+s.str());
	s.str("");
	outputString += node;
	if(value == (-2147483647))
		outputString+=",-Infinity,";
	else if(value == 2147483647)
		outputString+=",Infinity,";
	else{
		s<<fixed<<value;
		outputString+= ","+s.str() +",";
	}
	s.str("");
	if(alpha == (-2147483647))
		outputString+="-Infinity,";
	else if(alpha == 2147483647)
		outputString+="Infinity,"; 
	else{
		s<<fixed<<alpha;
		outputString+= s.str() +",";
	}
	s.str("");
	if(beta == (-2147483647))
		outputString+="-Infinity\n";
	else if(beta == 2147483647)
		outputString+="Infinity\n"; 
	else{
		s<<fixed<<beta;
		outputString+= s.str() +"\n";
	}
	s.str("");
	ofile<<outputString;
}

string printBoard(int totalNodes, char board[8][8],fstream &ofile){

	string outputString;
	for(int i=0;i<totalNodes;i++){
		for(int j=0;j<totalNodes;j++){
			outputString += board[i][j];
		}
		outputString += "\n";
	}
	return outputString;
}

void prepareOutputFile(string board,fstream &ifile){
	
	ifile.open("inter.txt",ios::in);
	ofstream newfile;
	newfile.open("output.txt",ios::out);
	newfile<<board;
	char ch;
	while(!ifile.eof())
	{
		ifile.get(ch);
		newfile<<ch;
	}
	ifile.close();
	newfile.close();
}

pair<int,int> traverseLeft(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){
	int count=0;
	while(j>0 && boardPosition[i][--j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(j<0)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else{
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);	
}

pair<int,int> traverseRight(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){

	int count=0;
	while( j<totalNodes-1 && boardPosition[i][++j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(j > totalNodes-1)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);	
}

pair<int,int> traverseUp(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){

	int count=0;
	while( i>0 && boardPosition[--i][j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(i<0)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j); 
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

pair<int,int> traverseDown(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){

	int count=0;
	while( i<totalNodes-1 && boardPosition[++i][j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(i>totalNodes-1)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

pair<int,int> traverseDiagonalRightUp(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){
	int count=0;
	while(i>0 && j<totalNodes-1 && boardPosition[--i][++j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(i<0 || j>totalNodes-1)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

pair<int,int> traverseDiagonalRightDown(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){
	int count=0;
	while( j<totalNodes-1 && i<totalNodes-1 && boardPosition[++i][++j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(j>totalNodes-1 || i>totalNodes-1)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

pair<int,int> traverseDiagonalLeftUp(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){
	int count=0;
	while(j>0 && i>0 && boardPosition[--i][--j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(i<0 || j<0)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

pair<int,int> traverseDiagonalLeftDown(char boardPosition[8][8], char player, char oppPlayer,int i,int j,int totalNodes){
	int count=0;
	while( j>0 && i<totalNodes && boardPosition[++i][--j] == oppPlayer){
		count++;
	}
	if(count>0){
		if(!(j<0 || i>totalNodes-1)){
			if(boardPosition[i][j] == player)
				return make_pair(-1,-1);
			if(boardPosition[i][j] == '*')
				return make_pair(i,j);
		} else {
			return make_pair(-1,-1);
		}
	}
	return make_pair(-1,-1);
}

bool isGameEnded(char boardPosition[8][8],int totalNodes){
	bool gameOver = true;
	for(int i=0;i<totalNodes;i++){
		for(int j=0;j<totalNodes;j++){
			if(boardPosition[i][j] == '*'){
				gameOver = false;
				break;
			}
		}
	}
	return gameOver;
}

int getScore(int totalNodes,char tempBoard[8][8],char player,char oppPlayer,int evaluationValue[8][8]){
	int playerScore = 0;
	int oppScore = 0;
	for(int i=0;i<totalNodes;i++){
		for(int j=0;j<totalNodes;j++){
			if(tempBoard[i][j] == player){
				playerScore += evaluationValue[i][j];
			} else if(tempBoard[i][j] == oppPlayer){
				oppScore += evaluationValue[i][j];
			}
		}
	}
	int evaluatedWeight = playerScore - oppScore;
	return evaluatedWeight;
}

map<pair<int,int>,int>  flipPieces(char player,char oppPlayer,list<pair<int,int> > evaluatedMoves, int evaluationValue[8][8],int totalNodes,char boardPosition[8][8]){

	char tempBoard[8][8];
	map<pair<int,int>,int> evaluatedWeightList;
	list<pair<int,int> > checkValue;
	list<pair<int,int> > :: iterator logit;
	int i,j,x,y;
	for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){
		for(int k=0;k<totalNodes;k++){
			for(int l=0;l<totalNodes;l++){
				tempBoard[k][l] = boardPosition[k][l];
			}
		}
		i = (*logit).first;
		j = (*logit).second;
		tempBoard[i][j] = player;
		for(int rowdelta = -1; rowdelta <= 1; rowdelta++){
			for(int coldelta = -1; coldelta <= 1; coldelta++){ 
			   /* Don't check off the board, or the current square */
				 if(i + rowdelta < 0 || i + rowdelta >= totalNodes ||
					 j + coldelta < 0 || j + coldelta >= totalNodes || 
									   (rowdelta==0 && coldelta== 0))
				 continue;

			   /* Now check the square */
				if(tempBoard[i + rowdelta][j + coldelta] == oppPlayer){
					 /* If we find the opponent, search in the same direction */
					 /* for a player counter                                  */
					 x = i + rowdelta;        /* Move to opponent */
					 y = j + coldelta;        /* square           */

					 for(;;)
					 {
					   x += rowdelta;           /* Move to the      */
					   y += coldelta;           /* next square      */ 

						/* If we are off the board give up */
					   if(x < 0 || x >= totalNodes || y < 0 || y >= totalNodes)
						 break;
 
						/* If the square is blank give up */
					   if(tempBoard[x][y] == '*')
						 break;

						/* If we find the player counter, go backwards from here */
						/* changing all the opponents counters to player         */
						if(tempBoard[x][y] == player){
							while(tempBoard[x-=rowdelta][y-=coldelta]==oppPlayer) /* Opponent? */
							tempBoard[x][y] = player;    /* Yes, change it */
							break;                     /* We are done    */
						} 
					}
				}
			}
		}
		evaluatedWeightList[make_pair(i,j)] = getScore(totalNodes,tempBoard,player,oppPlayer,evaluationValue);
	}
	return evaluatedWeightList;
}

vector<vector<char> >  returnTempBoard(char player,char oppPlayer,int totalNodes,vector<vector<char> > boardPosition,int xcord,int ycord){

	char tempBoard[8][8];
	vector<vector<char> > evaluatedWeightList(8,vector<char>(8));
	list<pair<int,int> > checkValue;
	int i,j,x,y;
		for(int k=0;k<totalNodes;k++){
			for(int l=0;l<totalNodes;l++){
				tempBoard[k][l] = boardPosition[k][l];
			}
		}
		i = xcord;
		j = ycord;
		tempBoard[i][j] = player;
		for(int rowdelta = -1; rowdelta <= 1; rowdelta++){
			for(int coldelta = -1; coldelta <= 1; coldelta++){ 
			   /* Don't check off the board, or the current square */
				 if(i + rowdelta < 0 || i + rowdelta >= totalNodes ||
					 j + coldelta < 0 || j + coldelta >= totalNodes || 
									   (rowdelta==0 && coldelta== 0))
				 continue;

			   /* Now check the square */
				if(tempBoard[i + rowdelta][j + coldelta] == oppPlayer){
					 /* If we find the opponent, search in the same direction */
					 /* for a player counter                                  */
					 x = i + rowdelta;        /* Move to opponent */
					 y = j + coldelta;        /* square           */

					 for(;;)
					 {
					   x += rowdelta;           /* Move to the      */
					   y += coldelta;           /* next square      */ 

						/* If we are off the board give up */
					   if(x < 0 || x >= totalNodes || y < 0 || y >= totalNodes)
						 break;
 
						/* If the square is blank give up */
					   if(tempBoard[x][y] == '*')
						 break;

						/* If we find the player counter, go backwards from here */
						/* changing all the opponents counters to player         */
						if(tempBoard[x][y] == player){
							while(tempBoard[x-=rowdelta][y-=coldelta]==oppPlayer) /* Opponent? */
							tempBoard[x][y] = player;    /* Yes, change it */
							break;                     /* We are done    */
						} 
					}
				}
			}
		}
		for(int i=0;i<totalNodes;i++){
			for(int j=0;j<totalNodes;j++){
				evaluatedWeightList[i][j] = tempBoard[i][j];
			}
		}
	return evaluatedWeightList;
}

string nextMove(char player,char oppPlayer,int totalNodes,char boardPosition[8][8],int i, int j){
	
	string outputString;
	int x,y;
	char tempBoard[8][8];
	for(int k=0;k<totalNodes;k++){
			for(int l=0;l<totalNodes;l++){
				tempBoard[k][l] = boardPosition[k][l];
			}
	}
	tempBoard[i][j] = player;
	for(int rowdelta = -1; rowdelta <= 1; rowdelta++){
		for(int coldelta = -1; coldelta <= 1; coldelta++){ 
			/* Don't check off the board, or the current square */
				if(i + rowdelta < 0 || i + rowdelta >= totalNodes ||
					j + coldelta < 0 || j + coldelta >= totalNodes || 
									(rowdelta==0 && coldelta== 0))
				continue;

			/* Now check the square */
			if(tempBoard[i + rowdelta][j + coldelta] == oppPlayer){
					/* If we find the opponent, search in the same direction */
					/* for a player counter                                  */
					x = i + rowdelta;        /* Move to opponent */
					y = j + coldelta;        /* square           */

					for(;;)
					{
					x += rowdelta;           /* Move to the      */
					y += coldelta;           /* next square      */ 

					/* If we are off the board give up */
					if(x < 0 || x >= totalNodes || y < 0 || y >= totalNodes)
						break;
 
					/* If the square is blank give up */
					if(tempBoard[x][y] == '*')
						break;

					/* If we find the player counter, go backwards from here */
					/* changing all the opponents counters to player         */
					if(tempBoard[x][y] == player){
						while(tempBoard[x-=rowdelta][y-=coldelta]==oppPlayer) /* Opponent? */
						tempBoard[x][y] = player;    /* Yes, change it */
						break;                     /* We are done    */
					} 
				}
			}
		}
	}

	for(int i=0;i<totalNodes;i++){
		for(int j=0;j<totalNodes;j++){
			outputString += tempBoard[i][j];
		}
		outputString += "\n";
	}
	return outputString;
}
pair<int,int> bestMove(char player,char oppPlayer, list<pair<int,int> > evaluatedMoves,int evaluationValue[8][8],map<int,string> boardCoordinates,int totalNodes,char boardPosition[8][8]){
	
	int maxScore;
	pair<int,int> bestMovePossible;
	bool valueChanged = false;
	map<pair<int,int>,int> evaluatedWeightList = flipPieces(player,oppPlayer,evaluatedMoves,evaluationValue,totalNodes,boardPosition);
	for (map<pair<int,int>,int>::iterator it=evaluatedWeightList.begin(); it!=evaluatedWeightList.end(); ++it){
		maxScore = (*it).second;
		break;
	}
	for (map<pair<int,int>,int>::iterator it=evaluatedWeightList.begin(); it!=evaluatedWeightList.end(); ++it){
		if(maxScore != (*it).second){
			valueChanged = true;
			break;
		}
	}
	if(valueChanged){
		for (map<pair<int,int>,int>::iterator it=evaluatedWeightList.begin(); it!=evaluatedWeightList.end(); ++it){
			if(maxScore < (*it).second){
				maxScore = (*it).second;
				bestMovePossible = make_pair((*it).first.first,(*it).first.second);
			}
		}

	} else {
		typedef pair<int,int> P;
		priority_queue<P,vector<P>,compareIndex> bestMoveQueue;
		for (map<pair<int,int>,int>::iterator it=evaluatedWeightList.begin(); it!=evaluatedWeightList.end(); ++it){
			bestMoveQueue.push(make_pair((*it).first.first,(*it).first.second));
		}
		bestMovePossible = bestMoveQueue.top();
	}
	return bestMovePossible;
}

list<pair<int,int> > legalMoves(char player,char boardPosition[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer){

	list<pair<int,int> > validMoves;
	list<pair<int,int> > possibleNewMoves;
	for(int i=0;i<totalNodes;i++){
		for(int j=0;j<totalNodes;j++){
			if(boardPosition[i][j] == player){
				validMoves.push_front(traverseLeft(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseRight(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseUp(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseDown(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseDiagonalRightUp(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseDiagonalRightDown(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseDiagonalLeftUp(boardPosition,player,oppPlayer,i,j,totalNodes));
				validMoves.push_front(traverseDiagonalLeftDown(boardPosition,player,oppPlayer,i,j,totalNodes));
			}
		}
	}
	
	list<pair<int,int> > ::iterator logit;
	for(logit = validMoves.begin();logit!= (validMoves).end();logit++){
		if(!(((*logit).first < 0) || ((*logit).second < 0))){
			possibleNewMoves.push_front(make_pair((*logit).first,(*logit).second));
		}
	}
	possibleNewMoves.sort();
	possibleNewMoves.unique();
	return possibleNewMoves;
}

string greedyTechnique(char player,char boardPosition[8][8], int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer){

	string outputString = "";
	list<pair<int,int> > evaluatedMoves;
	bool gameOver = isGameEnded(boardPosition,totalNodes);
	if(gameOver){
		for(int i=0;i<totalNodes;i++){
			for(int j=0;j<totalNodes;j++){
				outputString += boardPosition[i][j];
			}
			outputString += "\n";
		}
		return outputString;
	}	
	evaluatedMoves = legalMoves(player,boardPosition,boardCoordinates,totalNodes,oppPlayer);
	if(evaluatedMoves.size() == 0){
		for(int i=0;i<totalNodes;i++){
			for(int j=0;j<totalNodes;j++){
				outputString += boardPosition[i][j];
			}
			outputString += "\n";
		}
		return outputString;
	} else {
		pair<int,int> bestMovePosition = bestMove(player,oppPlayer,evaluatedMoves,evaluationFunction,boardCoordinates,totalNodes,boardPosition);
		outputString += nextMove(player,oppPlayer,totalNodes,boardPosition,bestMovePosition.first,bestMovePosition.second);
	}
	return outputString;
}

int maxMove(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int maxX,int maxY,fstream &ofile){
	list<pair<int,int> > ::iterator logit;
	vector<vector<char> > tempBoard(8,vector<char>(8));
	ostringstream s;
	int bestValue = -numeric_limits<int>::max();
	char nextBoard[8][8];
	list<pair<int,int> > evaluatedMoves;
	int i=0,j=0,value = -numeric_limits<int>::max();
	
	for(i=0;i<totalNodes;i++){
		for(j=0;j<totalNodes;j++){
			nextBoard[i][j] = boardPosition[i][j];
		}
	}
	if(depth>=cutOffDepth){
		if(depth==1){
			value = getScore(totalNodes,nextBoard,player,oppPlayer,evaluationFunction);
			printLog(boardCoordinates[maxY],maxX,depth,value,ofile);
			return value;
		} else {
			return getScore(totalNodes,nextBoard,player,oppPlayer,evaluationFunction);
		}
	} else {
		evaluatedMoves = legalMoves(player,nextBoard,boardCoordinates,totalNodes,oppPlayer);
		//if max has no valid moves
		if(evaluatedMoves.size() == 0){
			evaluatedMoves = legalMoves(oppPlayer,nextBoard,boardCoordinates,totalNodes,player);
			//if both sides have no more valid moves
			if(evaluatedMoves.size() == 0){
				value = getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
				printLog(boardCoordinates[maxY],maxX,depth,bestValue,ofile);
				return value;
			} else {
				printLog(boardCoordinates[maxY],maxX,depth,bestValue,ofile);

				//call to min
				value = minMove(oppPlayer,boardPosition,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,ofile);
				//print once value is returned
			
				printLog(boardCoordinates[9],-1,depth+1,value,ofile);
			
				if(value > bestValue){
					bestValue = value;
				}
			}
			printLog(boardCoordinates[maxY],maxX,depth,bestValue,ofile);
			return bestValue;
			} else {
			
			for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){
			
				printLog(boardCoordinates[maxY],maxX,depth,bestValue,ofile);

				tempBoard=returnTempBoard(player,oppPlayer,totalNodes,boardPosition,(*logit).first,(*logit).second);

				//call to min
				value = minMove(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,ofile);
				
				//print once value is returned
				if(depth+1 == cutOffDepth)
					printLog(boardCoordinates[(*logit).second],(*logit).first+1,depth+1,value,ofile);		
			
				if(value > bestValue){
					bestValue = value;
				}
			}
//			if((cutOffDepth<=2))
				printLog(boardCoordinates[maxY],maxX,depth,bestValue,ofile);
			return bestValue;
		}
	}
}

int maxMoveAlphaBeta(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int maxX,int maxY,int alpha,int beta,fstream &ofile){
	list<pair<int,int> > ::iterator logit;
	vector<vector<char> > tempBoard(8,vector<char>(8));
	char nextBoard[8][8];
	list<pair<int,int> > evaluatedMoves;
	int i=0,j=0,value=-numeric_limits<int>::max();
	
	for(i=0;i<totalNodes;i++){
		for(j=0;j<totalNodes;j++){
			nextBoard[i][j] = boardPosition[i][j];
		}
	}
	if(depth>=cutOffDepth){
		if(depth == 1){
			value = getScore(totalNodes,nextBoard,player,oppPlayer,evaluationFunction);
			printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,value,alpha,beta,ofile);
			return value;
		} else {
			return getScore(totalNodes,nextBoard,player,oppPlayer,evaluationFunction);
		}
	} else {
		evaluatedMoves = legalMoves(player,nextBoard,boardCoordinates,totalNodes,oppPlayer);
		//if max has no valid moves
		if(evaluatedMoves.size() == 0){
			evaluatedMoves = legalMoves(oppPlayer,nextBoard,boardCoordinates,totalNodes,player);

			//if both sides have no more valid moves
			if(evaluatedMoves.size() == 0){
				value = getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
				printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,value,alpha,beta,ofile);
				return value;
			} else {
				printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,alpha,alpha,beta,ofile);

				value = minMoveAlphaBeta(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,alpha,beta,ofile);
				//print once value is returned
				printLogAlphaBeta(boardCoordinates[9],-1,depth+1,value,alpha,beta,ofile);
				if(value >= beta ){
					//printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,value,alpha,beta,ofile);
					return value;
				}
				alpha = max(alpha,value);
				printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,alpha,alpha,beta,ofile);
				return value;
			}
		} else {
		
		for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){
			printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,alpha,alpha,beta,ofile);

			tempBoard=returnTempBoard(player,oppPlayer,totalNodes,boardPosition,(*logit).first,(*logit).second);

			//call to min
			value = minMoveAlphaBeta(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,alpha,beta,ofile);
			//print once value is returned

			if(depth+1 == cutOffDepth)
				printLogAlphaBeta(boardCoordinates[(*logit).second],(*logit).first+1,depth+1,value,alpha,beta,ofile);
			
			if(value >= beta ){
				if(depth+1 == cutOffDepth)
					printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,value,alpha,beta,ofile);
				return value;
			}
			alpha = max(alpha,value);
		}
		printLogAlphaBeta(boardCoordinates[maxY],maxX,depth,alpha,alpha,beta,ofile);
		return alpha;
		}
	}
}
int minMove(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int minX,int minY,fstream &ofile){

	list<pair<int,int> > ::iterator logit;
	vector<vector<char> > tempBoard(8,vector<char>(8));
	ostringstream s;
	int bestValue = numeric_limits<int>::max();
	char nextBoard[8][8];
	list<pair<int,int> > evaluatedMoves;
	int i=0,j=0,value = numeric_limits<int>::max();
	
	for(i=0;i<totalNodes;i++){
		for(j=0;j<totalNodes;j++){
			nextBoard[i][j] = boardPosition[i][j];
		}
	}
	if(depth>=cutOffDepth){
		if(depth==1){
			value = getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
			printLog(boardCoordinates[minY],minX,depth,value,ofile);
			return value;
		} else {
			return getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
		}
	} else {
		evaluatedMoves = legalMoves(player,nextBoard,boardCoordinates,totalNodes,oppPlayer);
		if(evaluatedMoves.size() == 0){
			evaluatedMoves = legalMoves(oppPlayer,nextBoard,boardCoordinates,totalNodes,player);
			if(evaluatedMoves.size() == 0){
				value = getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
				printLog(boardCoordinates[minY],minX,depth,value,ofile);
				return value;
			} else {
				printLog(boardCoordinates[minY],minX,depth,value,ofile);

				value = maxMove(oppPlayer,boardPosition,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,ofile);
			
				//print once value is returned
				printLog(boardCoordinates[9],-1,depth+1,value,ofile);

				if(value < bestValue){
					bestValue = value;
				}
			}
			printLog(boardCoordinates[minY],minX,depth,bestValue,ofile);
			return bestValue;
		} else {
			
			for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){
				printLog(boardCoordinates[minY],minX,depth,bestValue,ofile);	
				
				tempBoard=returnTempBoard(player,oppPlayer,totalNodes,boardPosition,(*logit).first,(*logit).second);

				//call to max
				value = maxMove(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,ofile);
			
				//print once value is returned
				if(depth+1==cutOffDepth)
					printLog(boardCoordinates[(*logit).second],(*logit).first+1,depth+1,value,ofile);

				if(value < bestValue){
					bestValue = value;
				}
			}
			//if((cutOffDepth<=2))
				printLog(boardCoordinates[minY],minX,depth,bestValue,ofile);
			return bestValue;
		}
	}
}

void minimax(char player,char boardPosition[8][8], int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,fstream &ofile){
	
	list<pair<int,int> > :: iterator logit;
	int bestValue = -numeric_limits<int>::max();
	int val,xcord,ycord;
	string outputString ="";
	string finalString ="";
	string board ="";
	vector<vector<char> > tempBoard(8,vector<char>(8));
	vector<vector<char> > copyBoard(8,vector<char>(8));
	list<pair<int,int> > evaluatedMoves;
	int depth = 0;

	 if(isGameEnded(boardPosition,totalNodes)){
		board += printBoard(totalNodes,boardPosition,ofile);
		ofile<<"Node,Depth,Value\n";
		bestValue = getScore(totalNodes,boardPosition,player,oppPlayer,evaluationFunction);
		printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
		ofile.close();
		prepareOutputFile(board,ofile);
	} else {
		evaluatedMoves = legalMoves(player,boardPosition,boardCoordinates,totalNodes,oppPlayer);

		if(evaluatedMoves.size() == 0){
			
			board += printBoard(totalNodes,boardPosition,ofile);
			ofile<<"Node,Depth,Value\n";
			evaluatedMoves = legalMoves(oppPlayer,boardPosition,boardCoordinates,totalNodes,player);
			
			if(evaluatedMoves.size() == 0){
				bestValue = getScore(totalNodes,boardPosition,player,oppPlayer,evaluationFunction);
				printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
			} else {
				printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
				for(int i=0;i<totalNodes;i++){
					for(int j=0;j<totalNodes;j++){
						copyBoard[i][j] = boardPosition[i][j];
					}
				}
				
				//call to min
				val = minMove(oppPlayer,copyBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,ofile);

				if(val > bestValue){
					bestValue = val;
				}

				printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
			}
		} else {
			ofile<<"Node,Depth,Value\n";
			printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
			for(int i=0;i<totalNodes;i++){
				for(int j=0;j<totalNodes;j++){
					copyBoard[i][j] = boardPosition[i][j];
				}
			}
			for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){

				tempBoard = returnTempBoard(player,oppPlayer,totalNodes,copyBoard,(*logit).first,(*logit).second);
			
				//call to min

				val = minMove(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,ofile);

				if(val > bestValue){
					bestValue = val;
					xcord = (*logit).first;
					ycord = (*logit).second;
				}
				//if(cutOffDepth>2)
					//printLog(boardCoordinates[(*logit).second],(*logit).first+1,depth+1,bestValue,ofile);
				printLog(boardCoordinates[8],-1,depth,bestValue,ofile);
			}
			board = nextMove(player,oppPlayer,totalNodes,boardPosition,xcord,ycord);
		}
		ofile.close();
		prepareOutputFile(board,ofile);
	 }
}


void alphaBeta(char player,char boardPosition[8][8], int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,fstream &ofile){
	
	list<pair<int,int> > :: iterator logit;
	int bestValue = -numeric_limits<int>::max();
	int value = -numeric_limits<int>::max();
	int alpha = -numeric_limits<int>::max();
	int beta =numeric_limits<int>::max();
	int xcord,ycord;
	string board ="";
	vector<vector<char> > tempBoard(8,vector<char>(8));
	vector<vector<char> > copyBoard(8,vector<char>(8));
	list<pair<int,int> > evaluatedMoves;
	int depth = 0;

	 if(isGameEnded(boardPosition,totalNodes)){
		board +=printBoard(totalNodes,boardPosition,ofile);
		ofile<<"Node,Depth,Value,Alpha,Beta\n";
		bestValue = getScore(totalNodes,boardPosition,player,oppPlayer,evaluationFunction);
		printLogAlphaBeta(boardCoordinates[8],-1,depth,bestValue,alpha,beta,ofile);
		ofile.close();
		prepareOutputFile(board,ofile);
	} else {
		evaluatedMoves = legalMoves(player,boardPosition,boardCoordinates,totalNodes,oppPlayer);

		if(evaluatedMoves.size() == 0){
			board += printBoard(totalNodes,boardPosition,ofile);
			ofile<<"Node,Depth,Value,Alpha,Beta\n";
			evaluatedMoves = legalMoves(oppPlayer,boardPosition,boardCoordinates,totalNodes,player);

			//if no valid moves for both sides
			if(evaluatedMoves.size() == 0){
				bestValue = getScore(totalNodes,boardPosition,player,oppPlayer,evaluationFunction);
				printLogAlphaBeta(boardCoordinates[8],-1,depth,bestValue,alpha,beta,ofile);
			} else { // else make the pass node
				for(int i=0;i<totalNodes;i++){
					for(int j=0;j<totalNodes;j++){
						copyBoard[i][j] = boardPosition[i][j];
					}
				}

				printLogAlphaBeta(boardCoordinates[8],-1,depth,alpha,alpha,beta,ofile);
				
				//call to min
				value = minMoveAlphaBeta(oppPlayer,copyBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,alpha,beta,ofile);

				if(alpha > bestValue){
					bestValue = alpha;
				}
				alpha = max(value,alpha);
				printLogAlphaBeta(boardCoordinates[8],-1,depth,alpha,alpha,beta,ofile);
			}
		} else {
			for(int i=0;i<totalNodes;i++){
				for(int j=0;j<totalNodes;j++){
					copyBoard[i][j] = boardPosition[i][j];
				}
			}
			ofile<<"Node,Depth,Value,Alpha,Beta\n";
			printLogAlphaBeta(boardCoordinates[8],-1,depth,alpha,alpha,beta,ofile);
			for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){

				tempBoard = returnTempBoard(player,oppPlayer,totalNodes,copyBoard,(*logit).first,(*logit).second);
			
				//call to min
				value = minMoveAlphaBeta(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,alpha,beta,ofile);

				if(value > bestValue){
					bestValue = value;
					xcord = (*logit).first;
					ycord = (*logit).second;
				}

				alpha = max(alpha,value);

				printLogAlphaBeta(boardCoordinates[8],-1,depth,alpha,alpha,beta,ofile);
			}
			board += nextMove(player,oppPlayer,totalNodes,boardPosition,xcord,ycord);
		}
		ofile.close();
		prepareOutputFile(board,ofile);
	}	
}


int main()
{
	int taskNumber;
	int cutOffDepth;
	char player;
	char oppPlayer;
	int totalNodes = 8;
	char boardPosition[8][8];
	map<int,string> boardCoordinates;
	int i =0;
	boardCoordinates[i] = "a";
	boardCoordinates[++i] = "b";
	boardCoordinates[++i] = "c";
	boardCoordinates[++i] = "d";
	boardCoordinates[++i] = "e";
	boardCoordinates[++i] = "f";
	boardCoordinates[++i] = "g";
	boardCoordinates[++i] = "h";
	boardCoordinates[++i] = "root";
	boardCoordinates[++i] = "pass";

	int evaluationValue[8][8] = {
									{99,-8,8,6,6,8,-8,99},
									{-8,-24,-4,-3,-3,-4,-24,-8},
									{8,-4,7,4,4,7,-4,8},
									{6,-3,4,0,0,4,-3,6},
									{6,-3,4,0,0,4,-3,6},
									{8,-4,7,4,4,7,-4,8},
									{-8,-24,-4,-3,-3,-4,-24,-8},
									{99,-8,8,6,6,8,-8,99}
								};

	ifstream ifile;
	fstream ofile;
	ifile.open("input.txt", ios::in);

	ifile>>taskNumber;

	ifile>>player;
	((player == 'X')?oppPlayer='O':oppPlayer='X');
		
	ifile>>cutOffDepth;

	for(int i=0;i<totalNodes;i++){
		for (int j=0; j < totalNodes; j++){
			ifile >> boardPosition[i][j];
		}
	}

	ifile.close();

	switch (taskNumber){
	case 1: {
			string outputString="";
			ofile.open("output.txt",ios::out);
			outputString += greedyTechnique(player,boardPosition,evaluationValue,boardCoordinates,totalNodes,oppPlayer);
			ofile<<outputString;
			ofile.close();
			break;
			}
	case 2: {
			string outputString;
			ofile.open("inter.txt",ios::out);
			minimax(player,boardPosition,cutOffDepth,evaluationValue,boardCoordinates,totalNodes,oppPlayer,ofile);
			break;
			}
	case 3: {
			string outputString;
			ofile.open("inter.txt",ios::out);
			alphaBeta(player,boardPosition,cutOffDepth,evaluationValue,boardCoordinates,totalNodes,oppPlayer,ofile);
			break;
			}
	}
	return 0;
}


int minMoveAlphaBeta(char player,vector<vector<char> > boardPosition, int cutOffDepth, int evaluationFunction[8][8],map<int,string> boardCoordinates,int totalNodes,char oppPlayer,int depth,int minX,int minY,int alpha,int beta,fstream &ofile){

	list<pair<int,int> > ::iterator logit;
	vector<vector<char> > tempBoard(8,vector<char>(8));
	char nextBoard[8][8];
	list<pair<int,int> > evaluatedMoves;
	int i=0,j=0,value=numeric_limits<int>::max();
	
	for(i=0;i<totalNodes;i++){
		for(j=0;j<totalNodes;j++){
			nextBoard[i][j] = boardPosition[i][j];
		}
	}
	if(depth>=cutOffDepth){
		if(depth == 1){
			value = getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
			printLogAlphaBeta(boardCoordinates[minY],minX,depth,value,alpha,beta,ofile);
			return value;
		} else {
			return getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
		}
	} else {
		evaluatedMoves = legalMoves(player,nextBoard,boardCoordinates,totalNodes,oppPlayer);
		if(evaluatedMoves.size() == 0){
			evaluatedMoves = legalMoves(oppPlayer,nextBoard,boardCoordinates,totalNodes,player);
			//if no valid moves on both sides
			if(evaluatedMoves.size() == 0){
				value =  getScore(totalNodes,nextBoard,oppPlayer,player,evaluationFunction);
				printLogAlphaBeta(boardCoordinates[minY],minX,depth,value,alpha,beta,ofile);
				return value;
			} else {
				printLogAlphaBeta(boardCoordinates[minY],minX,depth,value,alpha,beta,ofile);

				value =  maxMoveAlphaBeta(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,-1,9,alpha,beta,ofile);

				//print once value is returned
				printLogAlphaBeta(boardCoordinates[9],-1,depth+1,value,alpha,beta,ofile);

				if(value <= alpha){

					return value;
				}
				beta = min(beta,value);
				printLogAlphaBeta(boardCoordinates[minY],minX,depth,beta,alpha,beta,ofile);
				return beta;
			}
		}
		else {
		for(logit = evaluatedMoves.begin();logit != evaluatedMoves.end();logit++){
			printLogAlphaBeta(boardCoordinates[minY],minX,depth,beta,alpha,beta,ofile);

			tempBoard=returnTempBoard(player,oppPlayer,totalNodes,boardPosition,(*logit).first,(*logit).second);

			//call to max
			value =  maxMoveAlphaBeta(oppPlayer,tempBoard,cutOffDepth,evaluationFunction,boardCoordinates,totalNodes,player,depth+1,(*logit).first+1,(*logit).second,alpha,beta,ofile);
			
			//print once value os returned
			if(depth+1 == cutOffDepth)
				printLogAlphaBeta(boardCoordinates[(*logit).second],(*logit).first+1,depth+1,value,alpha,beta,ofile);

			if(value <= alpha){
				if(depth+1 == cutOffDepth)
					printLogAlphaBeta(boardCoordinates[minY],minX,depth,value,alpha,beta,ofile);
				return value;
			}
			beta = min(beta,value);
		}
		printLogAlphaBeta(boardCoordinates[minY],minX,depth,beta,alpha,beta,ofile);
		return beta;
	}
	}
}
